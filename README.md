# Kernel-updates

This repository doesn't contain any kernel source. 

This will keep track of RDP thinbook related bugfixes and releases. 

The kernel 5.x.x version has the jack detection working for es8316 audio codec(this codec is present in 2nd gen devices only). 
The corresponding `HiFi.conf` are in `es8316` directory.

# Build

On Debian 9
 
`sudo apt install build-essential ccache bc kmod cpio flex cpio libncurses5-dev`

`wget https://kernel-src.tar.gz`

`tar xf kernel-src.tar.gz`

`cd kernel-src`

`patch -p1 --ignore-whitespace if-any-file.patch` (The wifi signal patch isn't require from Kernel 5.x.x)

`cp -v <from-previous-kernel> .`

`make olddefconfig`

`time make CC="ccache gcc" bindeb-pkg -j4`


## Patches in the current kernel

None

## Issues in the current kernel

1. Only right channel audio works, though it plays on both speakers (issue with rt5651 only, i.e 1st gen device)

# Previous kernels

4.14.13+, 4.19rc7

## Prebuilt kernel for es8316 and rt5651 based audio chipsets

[kernel-5.2-rc7](https://drive.google.com/file/d/1jtktWcfNSGX7dxh1Oe1QXQS57H69jkDS/view)
